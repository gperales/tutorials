# Accediendo al DOM y sus elementos

Como en otras liberías similares Quo nos permite manejarnos dentro del coumento HTML, pudiendo así recorrer sus elementos y realizar acciones sobre ellos. 

El namespace para acceder a Quo es:

	$$
	
Con este namespace podemos acceder a las funcionalidades de la librería.

## Obteniendo elementos

Supongamos que en nuest html tenemos la siguiente etiqueta

	<div>
		<button id="push-button" class="red" data-name="pulse" >PUSH</button>
	</div>
	
Tenemos varias maneras de acceder a este elemento:

1.	Por el tipo de elemento

		$$('button')
		
2.	Por su identificador

		$$('#push-button')
		
3. Por su clase

		$$('.red')

4. A través de su elemento padre.

		$$('div > .red')

Podemos encadenar tantas consultas como queramos ya que a un resultado de una consulta viene con los métodos propios de Quo.

### Búsqueda por selector

Con la función find de QuoJS podemos buscar un determinado selector dentro de un elemento. La función find() devuelve un array con todos los elementos que haya encontrado
	
	$$('div').find('button')
	
### Buscar su elemento padre

Con la función parent podemos acceder al elemento padre de otro
	
	$$('#button').parent()
	
### Siblings

Si con parent() conseguimos el elemento padre siblings nos devuelve todos los hermanos que tiene ese elemento. Si le pasamos un selector filtraría sus hermanos por ese selector en concreto

	$$('#push-button').siblings('form')

### Children

Funciona de manera similar al parent() pero a la inversa, pero al poder ser un resultado múltiple devuelve todos los hijos que tenga un elemento dentro de un array.
	
	$$('div').children('button')
	
### First

La funcion first devuelve solo el primer elemento que encuentre en una determinada consulta, por ejemplo nos serviría combinada con children(). En este ejemplo nos devolvería el primer boton que encontrase dentro de un div.
	
	$$('div').children('button').first()
	
### Last

Si hay un método para seleccionar el primer elemento de un resultado tiene que haber otro para seleccionar el último.

	$$('div').children('button').last()

### El elemento mas cercano

Tambíen nos es posible acceder al elemento mas cercano que tenga un elemento dentro del DOM
	
	$$('#push-button').closest('a')
	
### Recorriendo elementos

Cuando una consulta nos devuelve varios resultados dentro de un array nos es posible recorrelos y aplicarles un callback definido por nosotros. Por ejemplo que saque un alert por cada children que encuentre.
	
	$$('div').children().each(alert("Hola" ))