# Peticiones Ajax

Para gran multitud de peticiones que se deben realizar de manera asíncrona utilizamos Ajax, la manera de realizar peticiones Ajax desde JavaScript tradicional es mediante las complejas xmlHttpRequest, QuoJS proporciona una interfaz para realizar peticiones Ajax sencilla a la vez que flexible.

## Métodos

Los principales verbos rest tienen su función Ajax. Tan solo llamamos a la función que queramos, definiendo los parámetros, el callback y tipología de los datos enviados si se precisa.

Esta es la estructura a seguir:

	$$.get(url, [parameters], [callback], [mime-type])
	
Una petición get con Ajax básica para enviar un objeto con un solo parámetro a nuestro server:

	$.get('myserver.com',{param: "get1"})
	$$.post('myserver.com',{param: "post1"})
	$$.put('myserver.com',{param: "put1"})
	$$.delete('myserver.com',{param: "delete1"})
	
Especificando el formato JSON en el header.
	
	$$.json('myserver.com',{param: "jsonType"})
	
Si queremos crearnos un proxy también podemos establecer todos los parámetros de la llamada.
	
	$$.ajax({
    	type: 'POST',
    	url: 'http://www.myrestserver',
    	data: {user: 'exampleUser', pass: 'examplePassword'},
    	dataType: 'json',
    	async: true,
    	success: function(response) { console.log(response); },
    	error: function(xhr, type) { console.log(xhr, type) }
	});
	
En el campo dataType podemos especificar el tipo de dato que vamos a enviar (json,xml,html,text)

## Configurar las opciones de la llamada

También podemos establecer una configuración predeterminada para todoas las peticiones Ajax que realicemos. Con el método ajaxSettings podemos definir timeouts por defecto para estas peticiones, callbacks de error o éxito que queramos predeterminados.

	$$.ajaxSettings = {
		async: true,
    	success: { alert("Success!!"); },
    	error: { alert("Error!!"); },
    	timeout: 0
	};
