# Manipulating elements with Javascript

## count
As it has been explained in the chapter about data-attributes, there is a property that adds a counter next to the element when you add it to a HTML tag. This property is called `data-count`, whose value is the number that will appear in the counter. This can be also than with JavaScript.

To remember, here you have an example explaining how is this attribute in HTML stated: 

    <section>
        …
        <footer>
            <nav>
                <a href="#" data-icon="user" data-count="12"></a>
                <a href="#" data-icon="envelope" id="messages"></a>
                <a href="#" data-icon="globe"></a>
                <a href="#" data-icon="cog"></a>
            </nav>
        </footer>
    </section>
    
To do it using Javascript, the function you have to run is the following:

	Lungo.Element.count("#messages", 5);

The first parameter that is sent to the function is the DOM reference of the element that you want to add a counter. And the second parameter is the number that will be added to the element. With this HTML of the example, and running the function, here you have an article with those elements in a screen.


![image](../images/19-count.png)


## loading
You can also create an element called loading in order to represent the moments used to load data using the `data-loading` attribute. Another way to show the loading image is using JavaScript.

If you have this in the HTML:


    <section id="main" data-transition="">
        <header data-title="Actualizando..."></header>
        <article id="main-article">
            <div id="add_loading"></div>
        </article>
    </section>

In order to add this element using JavaScript you have to run this function:

	Lungo.Element.loading("#add_loading", 1);
	
The first parameter sent to the dunction is the DOM reference of the element that you want to add the loading element. And the second parameter is optional, and is used to add the class that you want for the element, in order to have the posibility to add customized.

Once you have run this functio, the screen will show:


![image](../images/19-loading.png)

## progress
As it has benn stated in previous chapters, a progress bar can be created using the `data-progress` attribute and as a value the number followed by "%". LungoJS allows you to change the value of the progress bar using Javascript. 

If, for example, you have this HTML:

    <section id="main" data-transition="">
        <article id="main-article" class="active">
            <div class="form">
                <div id="prg-example" data-progress="0%"></div>
            </div>
        </article>
    </section>

You can change the progress value to 25% the following way:

	Lungo.Element.progress("#prg-example", 25);

The first parameter sent to the function is the DOM reference of the element where there is the progress bar you want to modify. And the second parameter is the percentage you want to assign.

In the screen you will see:


![image](../images/19-data_progress.png)

## menu

As is has been introduced in the chapter about navigation, LungoJS gives us a navigation menu where you find some options. In order to state that menu you have to add to the navigation element the `data-control="menu"` element. In order to remember how to state it in HTML:

	<nav id="options" data-control="menu">
    	<a href="#" data-view-article="article1" data-icon="menu" data-label="Inicio"></a>
    	<a href="#" data-view-article="article2" data-icon="globe" data-label="Explorar"></a>
    	<a href="#" data-view-article="article3" data-icon="comments" data-label="Comentarios"></a>
    	<a href="#" data-view-article="article4" data-icon="user" data-label="Perfil"></a>
	</nav>


![image](../images/19-menu.png)

LungoJS gives us some functions about this manu:

### Show

In order to show that menu using JavaScript you have to run the following function:

	Lungo.Element.menu.show("options");

The parameter sent belongs to the "id" of the navigation element that has the `data-control=menu` attribute you want to show. 

### Hide
On the contrary, to hide the naveigation menu you have to run this function:

	Lungo.Element.menu.hide("options");

As you do to show, the parameter sent corresponds with the "id" of the navigation element that has the `data-control=menu` attribute you want to hide.
	
	
### Toggle
This function, on the contrary, is in charge of showing the menu in case of not being in the screen, or hiding it in case of being in the screen. The function to run is the following:

	Lungo.Element.menu.toggle("options");

The parameter sent is the "id " of the navigation element that you want to show or hide.

## article
### empty

LungoJS gives us the option about the article to empty. It is run through this function:

	Lungo.Article.clean("my-article", "user", "Title", "Description", "Refresh")

The article will have a nive interface indicating that it is empty.

The parameters you have to send are: the first one the "id" of th article, the second one an icon of LungoJS that is going to be displayed in the screen, the third one corresponds with the main text shown just below the icon, the fourth one is optional and corresponds with the description, and the fifth is also optional, when you raun it, a button with appear with the text of the parameter.  
Los parámetros que hay que enviar son: el primero la "id" del artículo, el segundo un icono de LungoJS que se va a mostrar en medio de la pantalla, el tercero corresponde con el texto principal que se muestra justo debajo del icono, el cuarto es opcional y corresponde con la descripción, y el quinto también es opcional, si se le envía saldrá un botón que tendrá de texto este parámetro.

Here you have an example of how the interface will be displayed:

![image](../images/19-clean_article.png)

