# Icons
LungoJS has many icons divided in cateogries. Next, you will find a list of those icons, followed by their names. In order to use them, the `data-icon` attribute has to be added to the HTML elements and as value you have to assign the name of the icon you want. An example that shows how to use the icon in a list:


	<li data-icon="remove"></span>
		<strong>Icono de un aspa</strong>
        <small>suma</small>
    </li>
    
## Simbols


### basics

![image](../images/08-item_basic1.png)
![image](../images/08-item_basic2.png)

### signs

![image](../images/08-symbol_sign1.png)
![image](../images/08-symbol_sign2.png)

### circles

![image](../images/08-symbol_circle1.png)
![image](../images/08-symbol_circle2.png)

### arrows

![image](../images/08-symbol_chevron.png)

### angles

![image](../images/08-symbol_angle1.png)
![image](../images/08-symbol_angle2.png)
![image](../images/08-symbol_angle3.png)

### clouds
![image](../images/08-symbol_cloud.png)

## Media

### output peripherals

![image](../images/08-media_output.png)

### types

![image](../images/08-media_type.png)

### volume

![image](../images/08-media_volume.png)

### actions

![image](../images/08-media_player1.png)
![image](../images/08-media_player2.png)

## Tools bar

### text

![image](../images/08-toolbar_text1.png)
![image](../images/08-toolbar_text2.png)
### alignment


![image](../images/08-toolbar_aligment.png)

### elements

![image](../images/08-toolbar_element1.png)
![image](../images/08-toolbar_element2.png)
![image](../images/08-toolbar_element3.png)

### views y sizes

![image](../images/08-toolbar_viewsize.png)

### edition

![image](../images/08-toolbar_editor1.png)
![image](../images/08-toolbar_editor2.png)
![image](../images/08-toolbar_editor3.png)

### others

![image](../images/08-toolbar_others1.png)
![image](../images/08-toolbar_others2.png)

## Social


### basics

![image](../images/08-social_basic.png)

### actions

![image](../images/08-social_actions1.png)
![image](../images/08-social_actions2.png)
### thumbs and hands

![image](../images/08-social_thumbshands.png)

### firms

![image](../images/08-social_signing1.png)
![image](../images/08-social_signing2.png)
![image](../images/08-social_signing3.png)

## Objects

### basics

![image](../images/08-item_basic1.png)
![image](../images/08-item_basic2.png)

### purchases

![image](../images/08-item_shop1.png)
![image](../images/08-item_shop2.png)

### buildings

![image](../images/08-item_building.png)

### transports

![image](../images/08-item_transport.png)

### doctors

![image](../images/08-item_medical.png)

### others
![image](../images/08-item_others1.png)
![image](../images/08-item_others2.png)
![image](../images/08-item_others3.png)
![image](../images/08-item_others4.png)