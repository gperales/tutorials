# Core
LungoJS offers a series of functionalities, which can be accessed through CoffeeScript. This functions are run in its core. 

## Logs

LungoJS offers the functionality of printing messages by console while you are trying the app in the mobile device. The function you have to run for it is the following:

	Lungo.Core.log(1, "Launched event");

As it can be seen in the example, the first one is a number from 1 to 3, which represents the type of message you want to show. The 1 is used to show other normal messages, the 2 to show warnings, and the 3 to indicate a error message. The second value of the function belongs to the text that will appear in the console. 

Here you have another two examples of the rest of messages types:

	Lungo.Core.log(2, "Warning!!");
	Lungo.Core.log(3, "Error!!");
	
## bind
This function allows you to produce another function in which you define its context. 

When you run this function:

	var finalText;

	finalText = Lungo.Core.bind(example, addText)(text);	
Actually, the function tunning belongs to the second value, whose "this" will be what belongs to the first value. In the example can be seen that this new function has the "text" value. 

Here you have the complete example:
  
    var example = "This is ";

    var addText = function(textToAdd) {
      var text, word, _i, _len;
      text = this;
      for (_i = 0, _len = textToAdd.length; _i < _len; _i++) {
        word = textToAdd[_i];
        text += " " + word;
      }
      return text;
    };

    var text = ["an", "example"];

Thus, when running the function shown at the beginning, the function addText will be run and its "this" will be "This is". In this case the variable "finalText" would have the value "This is an example".

## mix
This funcitonality allows to create an object through the attributes of other objects. In other words, the function gets a series of objects where all attributes are mixed, and it obtains an object with those attributes. In the case of having duplicate attributes in the objects, the value of this in the resulting object will be the last received. An example:

If you have these objects:

    first = {
        text    : "hello",
        version : 1.0
    };
  
    var second = {
        description : "world",
        version     : 1.1
    };
     
And you run the function:

	new_object = Lungo.Core.mix(first, second);
 
new_object will have this value:

    new_object = {
        text        : "hello",
        description : "world",
        version     : 1.1
    };
     
## isOwnProperty
Through this function, you can know if an object has an attribute or not. 

The function is run the following way:

	Lungo.Core.isOwnProperty(car, "wheels");

The first value belongs to the object used to know if it has or not an attribute, and the second value belongs to the attribute itself. 

Thus, in the function you check if the object car has the "wheels" attribute.

If for example "car" would have this value:

    var car = {
        wheels : 4,
        doors  : true
    };
	  
The function would return the boolean "true"

And on the contrary, if the varible "car" would be:

    var car = {
        windows : 6,
        doors   : true
    };

	  
The function would return the boolean "false".

## toType

Establishes the intern JavaScript [[Class]] of an object. Returns a "String" with that [[Class]].

The function is run this way:

	Lungo.Core.toType(data);

The sent value belongs to the object used to know its [[Class]].

For example if data would be in this way:

    var data = {
        windows : 6,
        doors   : true
    };
	  
The function would return "object"

If that is:
	
	var data = "Lungo"

The returned function would be "string" 

## toArray
This function transforms an array type object into a JavaScript array object. Below you have an example where this can be useful:

    var execute = function() {
        var args;
        return args = Lungo.Core.toArray(arguments);
    };

    var execute("a", "b", "c");

"args" now will be:
	
	args = ["a", "b", "c"];
	
## isMobile
Establishes if the environment where the application is being run is mobile or not. The function is run this way:

	Lungo.Core.isMobile();

## environment
Returns the information of the environment where the application is run. The function is run the following way:

	Lungo.Core.environment();

Here you have an example of what would be returned by this function if this is run in an iphone 5´s browser:

    var data = {
        browser  : 'WebKit/537.51.1',
        isMobile : true,
        os: {
            name    : 'ios',
            version : '7.0'
        },
        screen: {
            height : 568,
            width  : 320
        }
    };

	   
## findByProperty
This method checks if a list of objects has any of them that contains an attribute with a particular value.

The function is run this way:

	user = Lungo.Core.findByProperty(list, 'name', 'Lungo');

The first value belongs to a list of objects, for example:


    var list = [
        {
            name    : 'Lungo',
            twitter : 'lungojs'
        }, {
            name    : 'Quojs',
            twitter : 'quojs'
        }
    ];

The second value belongs to the attribute you want to look for from those objects. In this case "name".

And the third value correspond to the value you want to have the attribute that has been stated in the second parameter. In this case, "Lungo".

Thus, when running that function, you look for an object that is in that list and whose "name" is "Lungo". When running it, the user will be:

	user = {
    	name    : 'Lungo',
    	twitter : 'lungojs'
	};