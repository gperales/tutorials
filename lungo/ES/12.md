# Pull & refresh
LungoJS permite asociar esta funcionalidad a un artículo. El artículo que lleve esto tiene la característica de ser "empujado" hacia abajo, y dejar ver un texto y/o icono hasta que un proceso termine, entonces el artículo volverá a su normalidad. Está pensado especialmente para cuando se tiene un artículo con listas, y estos elementos son actualizados, entonces el usuario arrastraría el artículo hacia abajo y se actualizarían dichos elementos.

Así se vería un artículo con scroll normal:

![image](../images/12-normal.png)

y así una vez hecho "pull" en él mientras esta ejecutando la función para cargar el nuevo contenido:

![image](../images/12-action.png)

Para hacer que un artículo tenga esta característica hay que añadir el atributo `data-pull="user"` a la sección que contenga dicho artículo. El valor de dicho artículo corresponde con el nombre de un icono de LungoJS y se verá en el trozo de artículo que saldrá al arrastrar el artículo hacia abajo.

Aquí se muestra un ejemplo de como quedaría el HTML:

    <section id="section" data-pull="user" >
        <header data-title="Pull &amp; Refresh" data-back="chevron-left"></header>
        <article id="article" class="list">
            HELLO WORLD!
        </article>
    </section>

Una vez hecho esto, por medio de JavaScript hay que hacer lo siguiente:

	var pull;

    pull = new Lungo.Element.Pull("#main-article", {
        onPull: "Pull down to refresh",
        onRelease: "Release to get new data",
        onRefresh: "Refreshing...",
        callback: function() {
          alert("Pull & Refresh completed!");
          pull.hide();
        }
    });
    
Como se ve en el ejemplo, al crear la instancia del "pull" hay que enviarle dos parámetros, el primero es la referencia del DOM del artículo al que se quiere agregar la característica del pull & refresh, el segundo parámetro es un objeto que contiene cuatro atributos. El primero "onPull" es el texto que aparecerá en el cacho de artículo que va a quedar descubierto mientras se está "empujando" dicho artículo. El segundo atributo es "onRelease", que es el texto por el que se sustituirá el anterior una vez terminado de arrastrar el artículo. El tercer parámetro es "onRefresh" que es el texto que sustituirá al anterior cuando se deje de "empujar" el artículo y por tanto mientras se esta realizando la acción por la que se hace el pull & refresh y el último atributo, es el callback que es la función a realizar. El artículo volverá a la normalidad haciendo el .hide() de la instancia pull como se muestra en el ejemplo anterior.