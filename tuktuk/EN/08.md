# Madals and Show & Hide

## Modals

The modals are popuo windows used for interact with the user, asking some information as notification in order to inform about an event. In order to display the modals you have to check that the scipt of tuktuk is loaded:

	<script src="package/tuktuk.js"></script>
	
And, of coursem, you have to declase the modal you want to display.

### Default

![image](../images/08-modal-default.png)

The button to launch it:

	<button data-tuktuk-modal="default_modal">Default</button>

Modal´s template:

    <div id="default_modal" data-tuktuk="modal" class="column_5 active">
        <header>
            <h4 class="text thin">Default modal window</h4>
        </header>
        <article>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus nihil aut sit incidunt amet modi dolores a sed.
        </article>
        <footer>
            <button data-modal="close" class="anchor"><span class="icon remove"></span>Close</button>
        </footer>
    </div>
    
<strong>Remainder: the class active is which makes it visible</strong> 

### Modal with estyle

![image](../images/08-modal-styl.png)

Button to launch it:

	<button data-tuktuk-modal="big_modal">Styling modal</button>

Template of the Styled modal:

    <div id="big_modal" data-tuktuk="modal" class="column_8 active">
        <header class="bck alert">
            <h4 class="text thin inline">Header with <strong>.bck .alert</strong> class</h4>
            <button data-modal="close" class="transparent small on-right inline icon remove"></button>
        </header>
        <article class="text big">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus nihil aut sit incidunt amet modi dolores a sed.</article>
        <footer>
            <button data-modal="close" class="button large secondary"><span class="icon ok"></span>Accept</button>
            <button data-modal="close" class="button  alert"><span class="icon remove"></span>Cancel</button>
        </footer>
    </div>



## Show & Hide

Tuktuk allows you to show only a certain content in a desktop web application and not in a tablet or smartphone. For these cases, Tuktuk gives you some extra classes in order to make the job easier for you and to hide or show an element depending on your needs. 

    <div class="row text center devices">
        <div class="column_4">
            <span class="icon desktop show-screen"></span>
            <span class="icon desktop hide-screen" style="opacity:0.1;"></span>
        </div>
        <div class="column_4">
            <span class="icon tablet show-tablet"></span>
            <span class="icon tablet hide-tablet" style="opacity:0.1;"></span>
        </div>
        <div class="column_4">
            <span class="icon mobile-phone show-phone"></span>
            <span class="icon mobile-phone hide-phone" style="opacity:0.1;"></span>
        </div>
    </div>

   

