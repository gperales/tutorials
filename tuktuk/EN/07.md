# Icons y navigation

## Icono

Tuktuk has by default a collection of 250 vector icons with different themes. In order to use an icon you just have to use the class icon followed by the attribute with the name of the wanted icon. 

![image](../images/07-icons.png)

	    <div class="column_12">
            <span class="icon glass"></span>
            <span class="icon music"></span>
            <span class="icon search"></span>
            <span class="icon envelope"></span>
            <span class="icon heart"></span>
            <span class="icon star"></span>
            <span class="icon star-empty"></span>
            <span class="icon user"></span>
            <span class="icon film"></span>
            <span class="icon grid-large"></span>
            <span class="icon grid"></span>
            <span class="icon list-th"></span>
            <span class="icon ok"></span>
            <span class="icon remove"></span>
            <span class="icon zoom-in"></span>
            <span class="icon zoom-out"></span>
            <span class="icon off"></span>
            <span class="icon signal"></span>
            <span class="icon cog"></span>
            <span class="icon trash"></span>
            <span class="icon home"></span>
            <span class="icon file"></span>
            <span class="icon time"></span>
            <span class="icon road"></span>
            <span class="icon download"></span>
            <span class="icon download-circle"></span>
            <span class="icon upload-circle"></span>
            <span class="icon inbox"></span>
            <span class="icon play-circle"></span>
            <span class="icon repeat"></span>
            <span class="icon refresh"></span>
            <span class="icon list-alt"></span>
            <span class="icon lock"></span>
            <span class="icon flag"></span>
            <span class="icon headphones"></span>
            <span class="icon volume-off"></span>
            <span class="icon volume-down"></span>
            <span class="icon volume-up"></span>
            <span class="icon qrcode"></span>
            <span class="icon barcode"></span>
            <span class="icon tag"></span>
            <span class="icon tags"></span>
            <span class="icon book"></span>
            <span class="icon bookmark"></span>
            <span class="icon print"></span>
            <span class="icon camera"></span>
            <span class="icon font"></span>
            <span class="icon bold"></span>
            <span class="icon italic"></span>
            <span class="icon text-height"></span>
            <span class="icon text-width"></span>
            <span class="icon align-left"></span>
            <span class="icon align-center"></span>
            <span class="icon align-right"></span>
            <span class="icon align-justify"></span>
            <span class="icon list"></span>
            <span class="icon indent-left"></span>
            <span class="icon indent-right"></span>
            <span class="icon video"></span>
            <span class="icon picture"></span>
            <span class="icon pencil"></span>
            <span class="icon map-marker"></span>
            <span class="icon adjust"></span>
            <span class="icon tint"></span>
            <span class="icon edit"></span>
            <span class="icon share"></span>
            <span class="icon check"></span>
            <span class="icon move"></span>
            <span class="icon step-backward"></span>
            <span class="icon fast-backward"></span>
            <span class="icon backward"></span>
            <span class="icon play"></span>
            <span class="icon pause"></span>
            <span class="icon stop"></span>
            <span class="icon forward"></span>
            <span class="icon fast-forward"></span>
            <span class="icon step-forward"></span>
            <span class="icon eject"></span>
            <span class="icon chevron-left"></span>
            <span class="icon chevron-right"></span>
            <span class="icon plus-sign"></span>
            <span class="icon minus-sign"></span>
            <span class="icon remove-sign"></span>
            <span class="icon ok-sign"></span>
            <span class="icon question-sign"></span>
            <span class="icon info-sign"></span>
            <span class="icon screenshot"></span>
            <span class="icon remove-circle"></span>
            <span class="icon ok-circle"></span>
            <span class="icon ban-circle"></span>
            <span class="icon arrow-left"></span>
            <span class="icon arrow-right"></span>
            <span class="icon arrow-up"></span>
            <span class="icon arrow-down"></span>
            <span class="icon share-alt"></span>
            <span class="icon resize-full"></span>
            <span class="icon resize-small"></span>
            <span class="icon plus"></span>
            <span class="icon minus"></span>
            <span class="icon asterisk"></span>
            <span class="icon exclamation-sign"></span>
            <span class="icon gift"></span>
            <span class="icon leaf"></span>
            <span class="icon fire"></span>
            <span class="icon eye-open"></span>
            <span class="icon eye-close"></span>
            <span class="icon warning-sign"></span>
            <span class="icon plane"></span>
            <span class="icon calendar"></span>
            <span class="icon random"></span>
            <span class="icon comment"></span>
            <span class="icon magnet"></span>
            <span class="icon chevron-up"></span>
            <span class="icon chevron-down"></span>
            <span class="icon retweet"></span>
            <span class="icon shopping-cart"></span>
            <span class="icon folder-close"></span>
            <span class="icon folder-open"></span>
            <span class="icon resize-vertical"></span>
            <span class="icon resize-horizontal"></span>
            <span class="icon bar-chart"></span>
            <span class="icon twitter-sign"></span>
            <span class="icon facebook-sign"></span>
            <span class="icon camera-retro"></span>
            <span class="icon key"></span>
            <span class="icon cogs"></span>
            <span class="icon comments"></span>
            <span class="icon thumbs-up"></span>
            <span class="icon thumbs-down"></span>
            <span class="icon star-half"></span>
            <span class="icon heart-empty"></span>
            <span class="icon signout"></span>
            <span class="icon linkedin-sign"></span>
            <span class="icon pushpin"></span>
            <span class="icon external-link"></span>
            <span class="icon signin"></span>
            <span class="icon trophy"></span>
            <span class="icon github-sign"></span>
            <span class="icon upload"></span>
            <span class="icon lemon"></span>
            <span class="icon phone"></span>
            <span class="icon check-empty"></span>
            <span class="icon bookmark-empty"></span>
            <span class="icon phone-sign"></span>
            <span class="icon twitter"></span>
            <span class="icon facebook"></span>
            <span class="icon github"></span>
            <span class="icon unlock"></span>
            <span class="icon credit-card"></span>
            <span class="icon rss"></span>
            <span class="icon hdd"></span>
            <span class="icon bullhorn"></span>
            <span class="icon bell"></span>
            <span class="icon certificate"></span>
            <span class="icon hand-right"></span>
            <span class="icon hand-left"></span>
            <span class="icon hand-up"></span>
            <span class="icon hand-down"></span>
            <span class="icon left-sign"></span>
            <span class="icon right-sign"></span>
            <span class="icon up-sign"></span>
            <span class="icon down-sign"></span>
            <span class="icon globe"></span>
            <span class="icon wrench"></span>
            <span class="icon tasks"></span>
            <span class="icon filter"></span>
            <span class="icon briefcase"></span>
            <span class="icon fullscreen"></span>
            <span class="icon group"></span>
            <span class="icon link"></span>
            <span class="icon cloud"></span>
            <span class="icon beaker"></span>
            <span class="icon cut"></span>
            <span class="icon copy"></span>
            <span class="icon paper-clip"></span>
            <span class="icon save"></span>
            <span class="icon sign-blank"></span>
            <span class="icon menu"></span>
            <span class="icon list-ul"></span>
            <span class="icon list-ol"></span>
            <span class="icon strikethrough"></span>
            <span class="icon underline"></span>
            <span class="icon table"></span>
            <span class="icon magic"></span>
            <span class="icon truck"></span>
            <span class="icon pinterest"></span>
            <span class="icon pinterest-sign"></span>
            <span class="icon google-plus-sign"></span>
            <span class="icon google-plus"></span>
            <span class="icon money"></span>
            <span class="icon caret-down"></span>
            <span class="icon caret-up"></span>
            <span class="icon caret-left"></span>
            <span class="icon caret-right"></span>
            <span class="icon caret-sort"></span>
            <span class="icon caret-sort-down"></span>
            <span class="icon caret-sort-up"></span>
            <span class="icon columns"></span>
            <span class="icon envelope-alt"></span>
            <span class="icon linkedin"></span>
            <span class="icon undo"></span>
            <span class="icon legal"></span>
            <span class="icon dashboard"></span>
            <span class="icon comment-alt"></span>
            <span class="icon comments-alt"></span>
            <span class="icon bolt"></span>
            <span class="icon sitemap"></span>
            <span class="icon umbrella"></span>
            <span class="icon paste"></span>
            <span class="icon lightbulb"></span>
            <span class="icon exchange"></span>
            <span class="icon cloud-download"></span>
            <span class="icon cloud-upload"></span>
            <span class="icon user-md"></span>
            <span class="icon stethoscope"></span>
            <span class="icon suitcase"></span>
            <span class="icon bell-alt"></span>
            <span class="icon coffee"></span>
            <span class="icon food"></span>
            <span class="icon file-alt"></span>
            <span class="icon building"></span>
            <span class="icon hospital"></span>
            <span class="icon ambulance"></span>
            <span class="icon medkit"></span>
            <span class="icon fighter-jet"></span>
            <span class="icon beer"></span>
            <span class="icon h-sign"></span>
            <span class="icon sign"></span>
            <span class="icon double-angle-left"></span>
            <span class="icon double-angle-right"></span>
            <span class="icon double-angle-up"></span>
            <span class="icon double-angle-down"></span>
            <span class="icon angle-left"></span>
            <span class="icon angle-right"></span>
            <span class="icon angle-up"></span>
            <span class="icon angle-down"></span>
            <span class="icon desktop"></span>
            <span class="icon laptop"></span>
            <span class="icon tablet"></span>
            <span class="icon mobile-phone"></span>
            <span class="icon circle-blank"></span>
            <span class="icon quote-left"></span>
            <span class="icon quote-right"></span>
            <span class="icon spinner"></span>
            <span class="icon circle"></span>
            <span class="icon reply"></span>
            <span class="icon github-alt"></span>
            <span class="icon folder-close-alt"></span>
            <span class="icon folder-open-alt"></span>
            <small>Based on <a href="http://fortawesome.github.io/Font-Awesome/" target="_blank" class="text bold color theme">Font Awesome</a> Project</small>
        </div>


## Navigation

In order to use the navigation menus, Tuktuk makes it easy for you. you just have to add the navigation tag nav. 

![image](../images/07-navigation.png)

	<section class="bck color">
        <div class="row text center">
            <nav data-tuktuk="menu" class="column_12 padding text bold">
                <a href="#"><span class="icon camera"></span>Shots<small>(100)</small></a>
                <a href="#" class="active"><span class="icon heart"></span>Likes<small>(3)</small></a>
                <a href="#"><span class="icon group"></span>Following<small>(192)</small></a>
                <a href="#"><span class="icon star"></span>Debuts<small>(160)</small></a>
            </nav>
        </div>
    </section>
    
	<section class="bck light">
        <div class="row text center">
            <nav data-tuktuk="menu" class="column_12 padding text bold">
                <a href="#"><span class="icon camera"></span>Shots<small>(100)</small></a>
                <a href="#"><span class="icon heart"></span>Likes<small>(3)</small></a>
                <a href="#" class="active"><span class="icon group"></span>Following<small>(192)</small></a>
                <a href="#"><span class="icon star"></span>Debuts<small>(160)</small></a>
            </nav>
        </div>
    </section>