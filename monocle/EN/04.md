# Controllers
They are in charge of assuming the responsabilities of a particular context. It can be fixed to a DOM element (it can be all the body) or not. The one that orchestrate your app, as it is in charge of obtaining the data, processing the models and managing the views. 

## Definition of a controller

The controllers and the views are defined in a similar way, the difference lies in the instantiation. A controller usually instantiates at the time that the app is initiated; that´s way the last line of the controller is which generates the instance of the controller. 

	class Tasks extends Monocle.Controller

	controller = new Tasks "section#tasks"

## Same controller for different areas

It is possible to reuse the code of a controller in order to manage different areas that have a similar behaviour. In the following example you can see how the Tasks controller is instanciated over two different elements, a section and an aside. 

	controller_section = new Tasks "section#tasks"
	controller_aside = new Tasks "aside#tasks"

## Functionalities

A controller can capture events and define elements the same way that happens with the views, but, moreover, it has to be the one that obtains the data and provide it to them. 

	class Tasks extends Monocle.Controller
    	
		events:
			"change input": "onSearch"

		elements:
			"input"       : "searchInput"
			
    	constructor: ->
        	super

    	onSearch: ->
    		task = __Model.Task.findBy "name", @searchInput.val()
    		console.log task
